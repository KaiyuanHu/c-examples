# MuseDev C Examples

MuseDev is a code analysis system that aids developers during code review. Working in concert with repository hosting systems, such as BitBucket, Gitlab, and GitHub, Muse acts on each pull request analyzing the code for defects and posting the results as comments inline with the repository host. 

This repository contains everything needed to see Muse in action, including a simple C program and a set of branches that introduce bugs. Each branch can be used to create a pull request that Muse will analyze. Simply follow the steps for your repository host listed below.

### Running on GitHub cloud

 1. **Install the Muse app** : Follow the instructions at [https://docs.muse.dev/docs/github-cloud/installation/](https://docs.muse.dev/docs/github-cloud/installation/) to install the Muse app.

 2. **Import this repository into GitHub** : Follow the instructions at [https://help.github.com/en/github/importing-your-projects-to-github/importing-a-repository-with-github-importer](https://help.github.com/en/github/importing-your-projects-to-github/importing-a-repository-with-github-importer) to import this repository into Github. The clone URL of this repo is `https://gitlab.com/musedev/c-examples.git`.

 3. **Add the repo to muse through the app settings** : Navigate to [https://github.com/apps/muse-dev](https://github.com/apps/muse-dev) and select `Configure`. Then choose the GitHub account you wish to configure.  Under the `Repository Access` section, enable the repository created in step 2. 

 4. **Create a pull request** : Follow the instructions at [https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request) to create a pull request using one of the pre-configured branches listed below.
 
 5. **View the findings** : After a couple minutes, Muse will post comments on the pull request describing any flaws in the code that were uncovered during analysis. 
 

### Branches

- `all-bugs` : [https://gitlab.com/musedev/c-examples/compare/master...all-bugs](https://gitlab.com/musedev/c-examples/compare/master...all-bugs)

- `dead-store` : [https://gitlab.com/musedev/c-examples/compare/master...dead-store](https://gitlab.com/musedev/c-examples/compare/master...dead-store)

- `memory-leak` : [https://gitlab.com/musedev/c-examples/compare/master...memory-leak](https://gitlab.com/musedev/c-examples/compare/master...memory-leak)

- `null-dereference` : [https://gitlab.com/musedev/c-examples/compare/master...null-dereference](https://gitlab.com/musedev/c-examples/compare/master...null-dereference)

- `resource-leak` : [https://gitlab.com/musedev/c-examples/compare/master...resource-leak](https://gitlab.com/musedev/c-examples/compare/master...resource-leak)

